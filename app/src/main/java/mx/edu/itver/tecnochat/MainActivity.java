package mx.edu.itver.tecnochat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;

import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    public final static String EXTRA_DIP = "mx.edu.itver.ChatTecno.DIP";
    public final static String EXTRA_USR = "mx.edu.itver.ChatTecno.USR";
    public final static String EXTRA_PAS = "mx.edu.itver.ChatTecno.PAS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btnConectar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),ChatActivity.class);

                EditText edtDireccionIP = findViewById(R.id.edtDireccionIP);
                String direccionIP = edtDireccionIP.getText().toString();

                EditText edtUsuario = findViewById(R.id.edtUsuario);
                String usuario = edtUsuario.getText().toString();

                EditText editPassword = findViewById(R.id.editPassword);
                String password = editPassword.getText().toString();

                intent.putExtra(EXTRA_DIP,"192.168.1.106");
                intent.putExtra(EXTRA_USR,usuario);
                intent.putExtra(EXTRA_PAS,password);




                startActivity(intent);
            }
        });

    }
}
